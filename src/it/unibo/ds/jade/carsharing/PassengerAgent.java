package it.unibo.ds.jade.carsharing;

import it.unibo.ds.jade.behaviours.DoForever;
import it.unibo.ds.jade.behaviours.WaitForMessage;
import it.unibo.utils.SmartDateTimeParsing;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;

import static it.unibo.ds.jade.messages.JadeMessagesUtils.newACLMessage;

public class PassengerAgent extends CarSharingAgent {

    private int nNeededSeats;
    private Places leavingPlace;
    private Places destination;
    private Instant preferredArrivalInstant;
    private double preferredQuota;
    private Duration expectedDuration;

    private final List<AID> drivers = Arrays.asList(
            new AID("Alessandro", AID.ISLOCALNAME),
            new AID("Andrea", AID.ISLOCALNAME)
    );

    public PassengerAgent() {
        super(ROLE_PASSENGER);
    }

    protected void initialize(final Object... args) {
        initialize(
                Places.fromName(args[0].toString()),
                Places.fromName(args[1].toString()),
                Integer.parseInt(args[2].toString()),
                SmartDateTimeParsing.parseInstant(args[3].toString()),
                Double.parseDouble(args[4].toString()),
                SmartDateTimeParsing.parseDuration(args[5].toString())
        );
    }

    private void initialize(final Places startingPoint, final Places destination, final int nNeededSeats,
                            final Instant preferredArrivalInstant, final double preferredQuota,
                            final Duration expectedDuration) {
        this.leavingPlace = startingPoint;
        this.destination = destination;
        this.nNeededSeats = nNeededSeats;
        this.preferredArrivalInstant = preferredArrivalInstant;
        this.preferredQuota = preferredQuota;
        this.expectedDuration = expectedDuration;
    }

    @Override
    protected void setup() {
        super.setup();

        // TODO cleanup this code and add your own setup code here
        addBehaviour(new OneShotBehaviour() {
            @Override
            public void action() {
                send(newACLMessage(ACLMessage.REQUEST, "hello!", drivers));
                log("Sent message 'hello!' to agents %s", drivers);
            }
        });

        addBehaviour(
            new DoForever(
                new WaitForMessage(MessageTemplate.MatchPerformative(ACLMessage.INFORM)) {

                    @Override
                    protected void handleMessage(ACLMessage receivedMessage, MessageTemplate template) {
                        log("Received message '%s' from agent %s", receivedMessage.getContent(), receivedMessage.getSender().getLocalName());
                    }
                }
            )
        );
    }
}
