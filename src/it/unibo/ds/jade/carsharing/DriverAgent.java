package it.unibo.ds.jade.carsharing;

import it.unibo.ds.jade.behaviours.DoForever;
import it.unibo.ds.jade.behaviours.WaitForMessage;
import it.unibo.utils.SmartDateTimeParsing;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.time.Instant;

import static it.unibo.ds.jade.messages.JadeMessagesUtils.newReply;

public class DriverAgent extends CarSharingAgent {

    private Places leavingPlace;
    private Places destination;
    private int nTotalSeats;
    private int nReservedSeats;
    private Instant leavingInstant;
    private double totalCost;



    public DriverAgent() {
        super(ROLE_DRIVER);
    }

    protected void initialize(final Object... args) {
        initialize(
            Places.fromName(args[0].toString()),
            Places.fromName(args[1].toString()),
            Integer.parseInt(args[2].toString()),
            Double.parseDouble(args[3].toString()),
            SmartDateTimeParsing.parseInstant(args[4].toString()),
            Integer.parseInt(args[5].toString())
        );
    }

    private void initialize(final Places leavingPlace, final Places destination, final int nTotalSeats,
                            final double totalCost, final Instant leavingInstant, final int initiallyReservedSeats) {
        this.leavingPlace = leavingPlace;
        this.destination = destination;
        this.nTotalSeats = nTotalSeats;
        this.totalCost = totalCost;
        this.nReservedSeats = initiallyReservedSeats;
        this.leavingInstant = leavingInstant;
    }

    @Override
    protected void setup() {
        super.setup();

        // TODO cleanup this code and add your own setup code here
        addBehaviour(
            new DoForever(
                new WaitForMessage(MessageTemplate.MatchPerformative(ACLMessage.REQUEST)) {

                    @Override
                    protected void handleMessage(ACLMessage receivedMessage, MessageTemplate template) {
                        log("Received message '%s' from agent %s", receivedMessage.getContent(), receivedMessage.getSender().getLocalName());
                        final ACLMessage reply = newReply(receivedMessage, ACLMessage.INFORM, "hello to you, " + receivedMessage.getSender().getLocalName());
                        log("Answer with message '%s'", reply.getContent());
                        send(reply);
                    }
                }
            )
        );
    }
}
